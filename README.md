IceWM
=====

The original IceWM project source management is complete chaos.
As a result I have created this single REPO, which provides a
well managed, stable and easy-to-follow source tree. That can be
easily used for your package management source to icewm.

About IceWM
-----------

Look and Feel... The aim of IceWM is to have good 'Feel' and decent
'Look'. 'Feel' is much more important than 'Look' ...

Send bug reports, feedback, suggestions, ...   to: 

`    gitlab@BSDforge.net`

See also BUGS, TODO and the sites at:
-------------------------------------

* https://icewm.xyz/
* https://BSDforge.com/projects/x11-wm/icewm/

Information about compilation and installation can be found in [INSTALL](INSTALL.md).
